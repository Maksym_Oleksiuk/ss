namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nahui : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.Friends");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendsId = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
