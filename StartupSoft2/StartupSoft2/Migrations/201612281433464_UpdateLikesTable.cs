namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLikesTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Likes", "UserId", c => c.String());
            DropColumn("dbo.Likes", "UsserId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Likes", "UsserId", c => c.String());
            DropColumn("dbo.Likes", "UserId");
        }
    }
}
