namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LikesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        LikeId = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        UsserId = c.String(),
                    })
                .PrimaryKey(t => t.LikeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Likes");
        }
    }
}
