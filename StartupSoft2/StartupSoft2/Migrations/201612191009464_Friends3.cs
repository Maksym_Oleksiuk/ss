namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Friends3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friends3",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendsId = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Friends3");
        }
    }
}
