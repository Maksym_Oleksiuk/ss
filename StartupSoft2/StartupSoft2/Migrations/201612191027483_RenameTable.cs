namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Friends3", newName: "Friends");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Friends", newName: "Friends3");
        }
    }
}
