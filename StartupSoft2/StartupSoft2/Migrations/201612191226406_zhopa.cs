namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zhopa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Friends",
                c => new
                    {
                        FriendId = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendsId = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.FriendId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Friends");
        }
    }
}
