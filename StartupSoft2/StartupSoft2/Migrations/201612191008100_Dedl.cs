namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Dedl : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.NewFriendsIntIds");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.NewFriendsIntIds",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendsId = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
