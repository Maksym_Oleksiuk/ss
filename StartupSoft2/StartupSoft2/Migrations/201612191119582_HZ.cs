namespace StartupSoft2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HZ : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.TestFriends");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TestFriends",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        FriendsId = c.String(),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
