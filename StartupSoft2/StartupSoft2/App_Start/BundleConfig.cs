﻿using System.Web;
using System.Web.Optimization;

namespace StartupSoft2
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.signalR-2.2.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/AddPostStyle.css"));
            bundles.Add(new StyleBundle("~/Content/Index").Include("~/Content/Index.css"));
            bundles.Add(new StyleBundle("~/Content/PostsView").Include("~/Content/PostsViewStyle.css"));

            bundles.Add(new ScriptBundle("~/bundles/angular-components").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-animate.js",
                        "~/Scripts/angular-messages.js",
                        "~/Scripts/angular-ui-router.js",
                        "~/Scripts/ng-input.js",
                        "~/Scripts/ui-bootstrap-custom-2.2.0.min.js",
                        "~/Scripts/ui-bootstrap-custom-tpls-2.2.0.min.js",
                        "~/Scripts/angular-toastr.tpls.js",
                        "~/Scripts/ng-croppie.js",
                        "~/Scripts/ng-file-upload-all.min.js",
                        "~/Scripts/ng-file-upload.min.js",
                        "~/Scripts/freewall.js",
                        "~/Scripts/angulargrid.min.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/PrintUsers").Include(
                "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                //"~/Scripts/angular-ui/ui-bootstrap.js",
                "~/Scripts/Angular/app.js",
                "~/Scripts/Angular/Home/appHome.js",
                "~/Scripts/Angular/Home/homeController.js",
                "~/Scripts/Angular/Home/homeService.js",
                "~/Scripts/Angular/Home/IndexActions.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/Angular/Posts").Include(
                "~/Scripts/Angular/Posts/appPosts.js",
                "~/Scripts/Angular/Posts/postsControllers.js",
                "~/Scripts/Angular/Posts/AddController.js",
                "~/Scripts/Angular/Posts/realtimeService.js",
                "~/Scripts/Angular/Posts/postsService.js",
                "~/Scripts/Angular/DateParse.js",
                "~/Scripts/Angular/Posts/ScrollDirective.js"
                ));
            /*bundles.Add(new ScriptBundle("~/bundles/Anguar/angular-ui").Include(
                "~/Scripts/Angular/angular-ui/ui-bootstrap-tpls.min.js",
                "~/Scripts/Angular/angular-ui/ui-bootstrap.min.js"
                ));*/
        }
    }
}
