﻿using Microsoft.AspNet.Identity.EntityFramework;
using StartupSoft2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StartupSoft2.DbContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //public DbSet<Friends> friends { get; set; }
        public DbSet<Friends> friends { get; set; }
        public DbSet<Posts> posts { get; set; }
        public DbSet<Likes> likes { get; set; }
        //public DbSet<TestFriends> test { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}