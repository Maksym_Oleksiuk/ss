﻿(function () {
    'use strict';


    function postsController(postsService, $uibModal, $scope) {
        postsService.getPosts();
        this.posts = postsService;
        //this.service = postsService;
        /*this.deletePost = function (post) {
            profileService.deletePost(post);
        }*/

        /*this.countLikes = function (post) {
            //console.log(post.likes);
            if (!post.likes)
                return 0;
            return post.likes.length;
        }*/
        this.open = function (post) {
            var modalInstance = $uibModal.open({
                templateUrl: '/Scripts/Angular/Posts/AddPost1.html',
                controller: ModalInstanceCtrl,
                resolve: {
                    post: function () {
                        return post;
                    }
                }
            });
            
            modalInstance.result.then(function (receivePost) {
                if (receivePost.PostId) {
                    postsService.editPost(post, receivePost);
                }
                else {
                    postsService.addPost(receivePost);
                }
            });
        };
        
        this.DelPost = function (post) {
            postsService.delPost(post);
        };
        this.Like = function (post) {
            postsService.like(post);
        }
        this.DisLike = function (post) {
            postsService.disLike(post);
        }
    }

    //    this.getNextPosts = function () {
    //        profileService.getPosts();
    //    }

    //    this.like = function (post) {
    //        console.log("controller +");
    //        profileService.Like(post);
    //    }

    //    this.unLike = function (post) {
    //        console.log("controller +");
    //        profileService.unLike(post);
    //    }

    //    this.getNextPosts();
    //}

    function ModalInstanceCtrl($scope, $uibModalInstance, post) {
        //$scope.post = post;
        var newPost = {
            PostId: null,
            Title: null,
            Description: null,
            Date: null,
            AuthorId: null,
            Name: null,
            Sorname: null
        }
        if (post) {
            $scope.title = post.Title;
            $scope.description = post.Description;
            newPost.PostId = post.PostId;
        }
        else {
            $scope.title = "";
            $scope.description = "";
        }
        

        //$scope.ok = function () {
            //$scope.post.Title = document.getElementById("title-post-edit").value;

            //$scope.post.Content = document.getElementById("content-post-edit").value;

            //$uibModalInstance.close($scope.post);
        //};
        $scope.AddPost = function () {
            //var newPost = {
            //    PostId: null,
            //    Title: $scope.title,
            //    Description: $scope.description,
            //    Date: null,
            //    AuthorId: null,
            //    Name: null,
            //    Sorname: null
            //}
            //postsService.addPost(newPost);
            newPost.Title = $scope.title;
            newPost.Description = $scope.description;
            if ($scope.title === "" && $scope.description === "") {
                angular.element('.error>p').css('display', 'block');
                angular.element('.modal-dialog').css('min-height', '550px');
                angular.element('.textArea').css('border', '1px solid red');
                angular.element('.titleArea').css('border', '1px solid red');
            }
            else
                if ($scope.title === "") {
                    angular.element('.error>p').css('display', 'block');
                    angular.element('.modal-dialog').css('min-height', '550px');
                    angular.element('.titleArea').css('border', '1px solid red');
                }
                else
                    if ($scope.description === "") {
                        angular.element('.error>p').css('display', 'block');
                        angular.element('.modal-dialog').css('min-height', '550px');
                        angular.element('.textArea').css('border', '1px solid red');
                    }
                    else {
                        $uibModalInstance.close(newPost);
                    }            
        }
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

    postsController.$inject = ['postsService', '$uibModal'];

    //ModalInstanceCtrl.$inject = [$modalInstance]

    angular
        .module('app.posts', [])
        .component('postsComponent', {
            templateUrl: '/Scripts/Angular/Posts/PostsView.html',
            controller: postsController,
            bindings: {}
        });
})();