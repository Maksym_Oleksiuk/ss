﻿(function () {
    function realtimeService($rootScope) {
        var setlike = $.connection.postsHub;
        $.connection.hub.start()
            .done(function () { console.log('Connected to Realtime hub !!!'); })
            .fail(function () { console.log('Could not Connect!'); });
        return {
            on: function (eventName, callback) {
                setlike.on(eventName, function (result) {
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback(result);
                        }
                    });
                });
            },
            off: function (eventName, callback) {
                setlike.off(eventName)//, function (result) {
                // console.log('done ­ off');
                //$rootScope.$apply(function () {
                // if (callback) {
                // callback(result);
                // }
                //});
                //});
            },
            invoke: function (methodName, args, callback) {
                $.connection.hub.start().done(function () {
                    setlike.invoke(methodName, args)
                    .done(function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                });
            }
        };
    };
    realtimeService.$inject = ['$rootScope'];
    angular
          .module('posts.realtime', [])
          .factory('realtimeService', realtimeService);
})();
