﻿(function () {
    'use strict';

    /*function inputChange($http, $location) {
        console.log("change test");
        return {
            restrict: 'AE',
            scope: true,
            link: function () {
                console.log("change test 2");
                angular.element(document.querySelector("#asd")).on("click", function () {
                    console.log("hover on button");
                });
            }
        };
    };*/
    function inputChange($window, postsService, $location) {
        
        return function (scope, element, attrs) {
            var action = false;
            angular.element($window).bind("scroll", function () {
                    //console.log(document.querySelector("#asd").offsetHeight);
                    if (this.pageYOffset >= (document.querySelector("#asd").offsetHeight - 700)) {
                        //scope.boolChangeClass = true;
                        //console.log('End');
                        if (action){
                            postsService.getPosts();
                            action = false;
                        }
                    } else {
                        action = true;
                        //scope.boolChangeClass = false;
                        //console.log(document.querySelector("#asd").offsetHeight)
                        //console.log('Header is in view.');
                    }
                
                //console.log(document.querySelector("#asd").offsetHeight);
                scope.$apply();
            });
        };
    }

    inputChange.$inject = ['$window', 'postsService', '$location'];
    angular.module('posts.directive', []).directive('inputChange', inputChange);
})()