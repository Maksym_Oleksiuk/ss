﻿(function () {
    angular.module('app.posts', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('posts', {
                url: "/Posts",
                template: '<posts-component></posts-component>',
                data: {
                }
            });
        }]);
})();

/*(function () {
    angular.module('app.posts', [])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('posts', {
                template: '<posts-component></posts-component>',
                controller: 'postsController',
                data: {
                }
            })
            .when('add', {
                template: '<add-component></add-component>',
                controller: 'addController',
                data: {
                }
            })
        }]);
})();*/