﻿
(function () {
    'use strict';

    function postsService($http, realtimeService) {
        var factory = {};
        var action = false;
        
        factory.controlLastPostId = -1;
        factory.lastPostId = 0;
        factory.posts = [];
        factory.userId;
        factory.getPosts = function () {
            //$http.get("/Posts/GetUserId").success(function (data) {
            //    factory.userId = data;
            //});
            //if (factory.controlLastPostId != factory.lastPostId) {
            //factory.controlLastPostId = factory.lastPostId;
            if (factory.controlLastPostId != factory.lastPostId) {
                $http.get("/Posts/ReturnPosts?lastPostId=" + factory.lastPostId).success(function (data) {
                    factory.userId = data.UserId;
                    factory.posts = factory.posts.concat(data.Posts);
                    factory.controlLastPostId = factory.lastPostId;
                    factory.lastPostId = factory.posts[factory.posts.length - 1].PostId;
                    console.log("send query");
                }).error(function (data) { console.log(data); });
            }
            //}
        }
        
        
        factory.editPost = function (post, receivePost) {
            $http.post("/Posts/EditPost", receivePost).success(function (data) {
                post.Title = receivePost.Title;
                post.Description = receivePost.Description;
            });
        }
        factory.delPost = function (post) {
            /*$http.post("/Posts/DeletePost", post).success(function (data) {
                factory.posts.splice(factory.posts.indexOf(post), 1);
            });*/
            var delPost = {
                Index: factory.posts.indexOf(post),
                otherPost: post
            }
            realtimeService.invoke("DeletePost", delPost);
        }
        
        realtimeService.on("delPost", function (index) {
            factory.posts.splice(index, 1);
        });
        factory.like = function (post) {
            if (!action) {
                action = true;
                if (factory.userId != "") {
                    realtimeService.invoke("Like", post);
                }
                action = false;
            }
        }
        factory.addPost = function (newPost) {
            if (newPost.Title != "" && newPost.Description != "") {
                $http.post("/Posts/AddPost", newPost).success(function (data) {
                    factory.posts.unshift(data);
                });
            }
        }
        factory.disLike = function (post) {
            if (!action) {
                action = true;
                if (factory.userId != "") {
                    realtimeService.invoke("DisLike", post);
                }
                action = false;
            }
        }
        realtimeService.on("liked", function (likedPost) {
            console.log("like");
            for (var i = 0; i < factory.posts.length; i++) {
                if (factory.posts[i].PostId === likedPost.PostId) {
                    factory.posts[i].NumberOfLikes = likedPost.NumberOfLikes;
                    if (likedPost.UserId === factory.userId || likedPost.UserId === null) {
                        factory.posts[i].UserId = likedPost.UserId;
                    }
                    break;
                }
            }
        });
        realtimeService.on("disliked", function (dislikedPost) {
            console.log("dislike");
            for (var i = 0; i < factory.posts.length; i++) {
                if (factory.posts[i].PostId === dislikedPost.PostId) {
                    factory.posts[i].NumberOfLikes = dislikedPost.NumberOfLikes;
                    if (dislikedPost.UserId === factory.userId) {
                        factory.posts[i].UserId = null;
                    }
                    break;
                }
            }
        });
        realtimeService.invoke("AddUserInGroup", "online");
        window.onbeforeunload = function () {
            realtimeService.invoke("DeleteUserFromGroup", "online");
        }
        return factory;
    };
    postsService.$inject = ['$http', 'realtimeService'];
    angular
      .module('posts.service', [])
      .factory('postsService', postsService);
})();