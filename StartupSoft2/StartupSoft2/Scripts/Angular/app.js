﻿angular.module('app', [
    'ui.bootstrap',
    'ngAnimate',
    'app.home',
    'posts.realtime',
    'home.service',
    'posts.service',
    'app.posts',
    'posts.directive'
])