﻿(function () {
    angular.module('app.home', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider.state('home', {
                url: "/Home",
                template: '<home-component></home-component>',
                data: {
                }
            });
        }]);
})();