﻿(function () {
    'use strict';

    function homeService($http) {
        var factory = {};
        var action = false;
        factory.users = [];
        factory.gethomeResults = function () {
            $http.get("/Home/ReturnUsers").success(function (data) {
                factory.users = data;
            });
        }
        factory.sendRequest = function (a) {
            if (!action) {
                action = true;
                $http.post("/Home/SendRequest", a).success(function (data) {
                    a.Status = data.Status;
                    a.FriendId = data.FriendId;
                    console.log(a);
                    action = false;
                }).error(function (data) { console.log(data); });
            }
        }
        factory.acceptRequest = function (a) {
            if (!action) {
                action = true;
                $http.post("/Home/AcceptRequest", a).success(function (data) {
                    a.Status = data.Status;
                    action = false;
                });
            }
        }
        factory.cancelRequest = function (a) {
            if (!action) {
                action = true;
                $http.post("/Home/CancelRequest", a).success(function (data) {
                    a.Status = data.Status;
                    action = false;
                });
            }
        }
        factory.deleteFromFriends = function (a) {
            if (!action) {
                action = true;
                $http.post("/Home/DeleteFromFriends", a).success(function (data) {
                    a.Status = data.Status;
                    a.FriendId = data.FriendId;
                    action = false;
                }).error(function (data) { console.log(data); });
            }
        }
        return factory;
    };
    homeService.$inject = ['$http'];
    angular
      .module('home.service', [])
      .factory('homeService', homeService);
})();