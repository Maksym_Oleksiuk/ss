﻿(function () {
    'use strict';


    function homeController(homeService) {
        homeService.gethomeResults();
        this.users = homeService;
        this.SendRequest = function (a) {
            homeService.sendRequest(a);
        }

        this.AcceptRequest = function (a) {
            homeService.acceptRequest(a);
        }

        this.CancelRequest = function (a) {
            homeService.cancelRequest(a);
        }

        this.DeleteFromFriends = function (a) {
            homeService.deleteFromFriends(a);
        }
        /*vm.getResults = function () {
            homeService.gethomeResults().success(function (data) {
                vm.users = data;
                console.log("get users");
            });
        }*/


        //vm.getResults();
    }


    homeController.$inject = ['homeService'];


    angular
        .module('app.home', [])
        .component('homeComponent', {
            templateUrl: '/Scripts/Angular/Home/1.html',
            controller: homeController,
            bindings: {


            }
        });
})()
