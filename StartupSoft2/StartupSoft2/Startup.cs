﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(StartupSoft2.Startup))]
namespace StartupSoft2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
