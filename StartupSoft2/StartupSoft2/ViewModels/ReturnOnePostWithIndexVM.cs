﻿using StartupSoft2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StartupSoft2.ViewModels
{
    public class ReturnOnePostWithIndexVM
    {
        public int Index { get; set; }
        public Posts otherPost { get; set; }
    }
}