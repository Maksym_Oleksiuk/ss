﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StartupSoft2.ViewModels
{
    public class JoinFriends
    {
        public string Name { get; set; }
        public string Sorname { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public int? FriendId { get; set; }
        public string Id { get; set; }
    }
}