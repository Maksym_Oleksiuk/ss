﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StartupSoft2.ViewModels
{
    public class JoinPosts
    {
        public string Name { get; set; }
        public string Sorname { get; set; }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public string AuthorId { get; set; }
        public int? NumberOfLikes { get; set; }
        public string UserId { get; set; }
    }
}