﻿using StartupSoft2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StartupSoft2.ViewModels
{
    public class JoinPostWithAuthorizeUserIdVM
    {
        public string UserId { get; set; }
        public IEnumerable<JoinPosts> Posts { get; set; }
    }
}