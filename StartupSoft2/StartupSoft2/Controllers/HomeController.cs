﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StartupSoft2.Models;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using StartupSoft2.ViewModels;
using StartupSoft2.DbContext;

namespace StartupSoft2.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        [AllowAnonymous]
        public ActionResult Index()
        {
            //string userId = User.Identity.GetUserId();
            //List<ApplicationUser> users = db.Users.ToList();
            return View();
        }
        
        public JsonResult ReturnUsers()
        {
            string userId = User.Identity.GetUserId();
            string query = "Select AspNetUsers.Id, asd.FriendId, Name, Sorname, Email, Status From AspNetUsers left join(Select * from Friends where UserId = '" + userId + "') as asd on AspNetUsers.Id = asd.FriendsId where AspNetUsers.Id != '" + userId + "';";
            IEnumerable<JoinFriends> data = db.Database.SqlQuery<JoinFriends>(query);
            return new JsonResult() { Data = data.ToList(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult SendRequest(JoinFriends a)
        {
            string tmp = User.Identity.GetUserId();
            if (a.FriendId == null)
            {
                Friends temp = new Friends { UserId = tmp, FriendsId = a.Id, Status = "OutgoingRequest" };
                db.friends.Add(temp);
                db.SaveChanges();
                a.FriendId = temp.FriendId;
                db.friends.Add(new Friends { UserId = a.Id, FriendsId = tmp, Status = "PendingRequest" });
                db.SaveChanges();
                a.Status = "OutgoingRequest";
            }
            else
            {
                AcceptRequest(a);
                a.Status = "friend";
            }
            return new JsonResult() { Data = a, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult AcceptRequest(JoinFriends a)
        {
            string userId = User.Identity.GetUserId();
            Friends friend = db.friends.Where(x => x.FriendId == a.FriendId).FirstOrDefault();
            friend.Status = "friend";
            friend = db.friends.Where(x => x.UserId == a.Id && x.FriendsId == userId).FirstOrDefault();
            friend.Status = "friend";
            db.SaveChanges();
            a.Status = "friend";
            return new JsonResult() { Data = a, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult CancelRequest(JoinFriends a)
        {
            string tmp = User.Identity.GetUserId();
            Friends friend = db.friends.Where(x => x.FriendId == a.FriendId).FirstOrDefault();
            friend.Status = null;
            if (a.Status == "friend")
            {
                friend = db.friends.Where(x => x.UserId == a.Id && x.FriendsId == tmp).FirstOrDefault();
                friend.Status = "OutgoingRequest";
            }
            a.Status = null;
            db.SaveChanges();
            return new JsonResult() { Data = a, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult DeleteFromFriends(JoinFriends a)
        {
            string userId = User.Identity.GetUserId();
            Friends DelFromFriends = db.friends.Where(x => x.FriendId == a.FriendId).FirstOrDefault();
            db.friends.Remove(DelFromFriends);
            DelFromFriends = db.friends.Where(x => x.UserId == a.Id && x.FriendsId == userId).FirstOrDefault();
            db.friends.Remove(DelFromFriends);
            db.SaveChanges();
            a.Status = null;
            a.FriendId = null;
            //return 1;
            return new JsonResult() { Data = a, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}