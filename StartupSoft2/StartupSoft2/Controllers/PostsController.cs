﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StartupSoft2.Models;
using Microsoft.AspNet.Identity;
using StartupSoft2.ViewModels;
using System.Collections;
using StartupSoft2.DbContext;

namespace StartupSoft2.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Posts
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AddPost(JoinPosts post)
        {
            string tmp = User.Identity.GetUserId();
            Posts addPost = new Posts { Date = DateTime.Now, Title = post.Title, Description = post.Description, AuthorId = tmp };
            ApplicationUser user = db.Users.Where(x => x.Id == tmp).FirstOrDefault();
            db.posts.Add(addPost);
            db.SaveChanges();
            post.PostId = addPost.PostId;
            post.AuthorId = addPost.AuthorId;
            post.Date = addPost.Date;
            post.Name = user.Name;
            post.Sorname = user.Sorname;
            return new JsonResult() { Data = post, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        //public JsonResult GetUserId()
        //{
        //    string userId = User.Identity.GetUserId();
        //    return new JsonResult() { Data = userId, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}
        [AllowAnonymous]
        [HttpGet]
        public JsonResult ReturnPosts(int lastPostId)
        {
            string query = "Select distinct AspNetUsers.Id, AspNetUsers.Name, AspNetUsers.Sorname, Posts.PostId, Posts.Title," +
                "Posts.Description, Posts.Date, Posts.AuthorId, LikesTable.NumberOfLikes, Likes2.UserId from Posts "
                + "Left Join AspNetUsers on Posts.AuthorId = AspNetUsers.Id "
                + "Left Join(select Distinct PostId, UserId, Count(PostId) OVER(PARTITION BY PostId) as NumberOfLikes from Likes)"
                + "as LikesTable on Posts.PostId = LikesTable.PostId "
                + "Left Join(select distinct PostId, UserId from Likes where UserId = '" + User.Identity.GetUserId() + "') as Likes2 on Posts.PostId = Likes2.PostId";
            if (lastPostId == 0)
                query += " order by Date desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY;";
            else
                query += " where Posts.PostId < " + lastPostId + " order by Date desc OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY;";
            JoinPostWithAuthorizeUserIdVM PostList = new JoinPostWithAuthorizeUserIdVM();
            PostList.UserId = User.Identity.GetUserId();
            PostList.Posts = db.Database.SqlQuery<JoinPosts>(query);
            PostList.Posts = PostList.Posts.ToList();
            //PostList = ListPost;
            //IEnumerable<JoinPosts> data = db.Database.SqlQuery<JoinPosts>(query);

            return new JsonResult() { Data = PostList, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            //List<Posts> posts = db.posts.OrderByDescending(x => x.Date).ToList();
            //return new JsonResult() { Data = posts, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        /*public void DeletePost(Posts post)
        {
            Posts DeletingPost = db.posts.Where(x => x.PostId == post.PostId).FirstOrDefault();
            db.posts.Remove(DeletingPost);
            db.SaveChanges();
        }*/
        [HttpPost]
        public void EditPost(Posts post)
        {
            string tmp = User.Identity.GetUserId();
            Posts EditPost = db.posts.Where(x => x.PostId == post.PostId).FirstOrDefault();
            EditPost.Title = post.Title;
            EditPost.Description = post.Description;
            db.SaveChanges();
            //return new JsonResult() { Data = post, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public string Like (JoinPosts LikedPost){
            string userId = User.Identity.GetUserId();
            Likes tmp = new Likes { UserId = userId, PostId = LikedPost.PostId };
            db.likes.Add(tmp);
            db.SaveChanges();
            return User.Identity.GetUserId();
        }
        public void DisLike(JoinPosts DisLikedPost)
        {
            string userId = User.Identity.GetUserId();
            Likes Disliked = db.likes.Where(x => x.PostId == DisLikedPost.PostId && x.UserId == userId).FirstOrDefault();
            db.likes.Remove(Disliked);
            db.SaveChanges();
        }
    }
}