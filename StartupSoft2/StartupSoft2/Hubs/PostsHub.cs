﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using StartupSoft2.Models;
using Microsoft.AspNet.Identity;
using StartupSoft2.ViewModels;
using StartupSoft2.DbContext;

namespace StartupSoft2.Hubs
{
    public class PostsHub : Hub
    {
        ApplicationDbContext db = new ApplicationDbContext();
        //public void AddPost(Posts post)
        //{
        //    string tmp = Context.User.Identity.GetUserId();
        //    JoinPosts newPost = new JoinPosts();
        //    Posts addPost = new Posts { Date = DateTime.Now, Title = post.Title, Description = post.Description, AuthorId = tmp };
        //    ApplicationUser user = db.Users.Where(x => x.Id == tmp).FirstOrDefault();
        //    db.posts.Add(addPost);
        //    db.SaveChanges();
        //    newPost.PostId = addPost.PostId;
        //    newPost.AuthorId = addPost.AuthorId;
        //    newPost.Date = addPost.Date;
        //    newPost.Name = user.Name;
        //    newPost.Sorname = user.Sorname;
        //    Clients.Group("online").NewPost(newPost);
        //    //return new JsonResult() { Data = post, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //}

        public void DeletePost(ReturnOnePostWithIndexVM postWithIndex)
        {
            //Posts DeletingPost = db.posts.Where(x => x.PostId == postWithIndex.otherPost.PostId).FirstOrDefault();
            db.posts.Remove(db.posts.Where(x => x.PostId == postWithIndex.otherPost.PostId).FirstOrDefault());
            db.likes.RemoveRange(db.likes.Where(x => x.PostId == postWithIndex.otherPost.PostId));
            db.SaveChanges();
            Clients.Group("online").delPost(postWithIndex.Index);
        }
        /*
        public void EditPost(Posts post)
        {
            string tmp = User.Identity.GetUserId();
            Posts EditPost = db.posts.Where(x => x.PostId == post.PostId).FirstOrDefault();
            EditPost.Title = post.Title;
            EditPost.Description = post.Description;
            db.SaveChanges();
            //return new JsonResult() { Data = post, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }*/
        public void Like(JoinPosts LikedPost)
        {
            if (LikedPost.NumberOfLikes == null)
                LikedPost.NumberOfLikes = 0;
            string id = Context.User.Identity.GetUserId();
            Likes tmp = new Likes { UserId = id, PostId = LikedPost.PostId };
            db.likes.Add(tmp);
            db.SaveChanges();
            LikedPost.NumberOfLikes++;
            LikedPost.UserId = id;
            
            Clients.Group("online").liked(LikedPost);   
        }
        public void DisLike(JoinPosts DisLikedPost)
        {
            string id = Context.User.Identity.GetUserId();
            Likes tmp = db.likes.Where(x => x.UserId == id && x.PostId == DisLikedPost.PostId).FirstOrDefault();
            db.likes.Remove(tmp);
            db.SaveChanges();
            DisLikedPost.NumberOfLikes--;
            if (DisLikedPost.NumberOfLikes == 0)
                DisLikedPost.NumberOfLikes = null;
            Clients.Group("online").disliked(DisLikedPost);
        }
        public void AddUserInGroup(string name)
        {
            Groups.Add(Context.ConnectionId, name);
        }
        public void DeleteUserFromGroup(string name)
        {
            Groups.Remove(Context.ConnectionId, name);
        }
    } 
}