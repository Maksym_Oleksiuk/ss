﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StartupSoft2.Models
{
    public class Likes
    {
        [Key]
        public int LikeId { get; set; }
        public int PostId { get; set; }
        public string UserId { get; set; }
    }
}