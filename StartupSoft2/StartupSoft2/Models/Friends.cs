﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StartupSoft2.Models
{
    public class Friends
    {
        [Key]
        public int FriendId { get; set; }
        public string UserId { get; set; }
        public string FriendsId { get; set; }
        public string Status { get; set; }
    }
}