﻿/*select PostId, LikeId, UserId, Count(PostId) OVER(PARTITION BY PostId) as NumberOfLikes from Likes Order By PostId;*/
Select AspNetUsers.Id, AspNetUsers.Name, AspNetUsers.Sorname, Posts.PostId, Posts.Title, Posts.Description, Posts.Date, Posts.AuthorId, LikesTable.NumberOfLikes, Likes2.UserId from Posts 
	Left Join AspNetUsers on Posts.AuthorId = AspNetUsers.Id 
	Left Join(select Distinct PostId, UserId, Count(PostId) OVER(PARTITION BY PostId) as NumberOfLikes from Likes) as LikesTable on Posts.PostId = LikesTable.PostId
	Left Join(select distinct PostId, UserId from Likes where UserId = '660a83de-09af-4c5a-b9fc-01203d625828') as Likes2 on Posts.PostId = Likes2.PostId
	order by Date OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY;
	/*select distinct PostId, UserId from Likes where UserId = '660a83de-09af-4c5a-b9fc-01203d625828';*/